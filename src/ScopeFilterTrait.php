<?php

namespace Triatoma\LaravelFilter;

use Triatoma\LaravelFilter\QueryFilter;

trait ScopeFilterTrait
{
	protected $request;

	public function scopeFilter($query, QueryFilter $filters)
	{
	    return $filters->apply($query);
	}
}